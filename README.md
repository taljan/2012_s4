# Projekt einrichten

### Voraussetzung
Java 1.8
MAVEN

### Mit git
1. Falls GIT bereits installtiert ist einfach den Befehl im Terminal:
`git clone git@bitbucket.org:taljan/2012_s4.git`, gewünschtem Verzeichnis ausführen.
 2. IDE öffnen und das Projekt importieren.
 3. In die Main Klasse gehen und in der Zeile 

 ```java
 Simulation.of(longestPath).simulate(args[0])
 Simulation.of(shortestPath).simulate(args[0])
 ```

 args[0] in den Dateinamen ändern.
 Beispiel:

 ```java
 Simulation.of(longestPath).simulate("example_data_01.txt")
 Simulation.of(shortestPath).simulate("exmaple_data_01.txt")
 ```
Weitere Beispiel Dateien sind im Hauptverzeichnis vorhanden.


### Program über das jar file ausführen.
1. Öffnen Sie das Terminal und gehen sie in den Ordner `/out/artifacts/2012_s4_jar`.
2. Dort den folgenden Befehl ausgeben: `java -jar 2012_s4.jar "../../../example_data_01.txt"`
3. `example_data_01.txt` mit weiteren Beispielen ersetzen.

PS: Beispiel 6 kann etwas länger dauern als die anderen Beispiele. Dort geht der Algorithmus
einen Ziemlich großen Graphen durch.