import data.Graph;
import data.Node;
import service.PathService;
import service.PrintService;
import service.impl.DataReaderService;
import service.impl.LongestPathImpl;
import service.impl.PrintServiceImpl;

import java.util.LinkedList;

public class Simulation {

    private PathService pathService;
    private PrintService printService;

    private Simulation(PathService pathService) {
        this.pathService = pathService;
        this.printService = new PrintServiceImpl();
    }

    public static Simulation of(PathService pathService) {
        return new Simulation(pathService);
    }

    /**
     * The simulation which starts the pathfinding in graph.
     * This method does all the data manipulation.
     * It reads a file and maps the data for the algorithm to a graph.
     * @param filename - file to search the path.
     */
    public void simulate(String filename) {
        Graph graph = DataReaderService.of().getData(filename);
        int[] dataDimension = DataReaderService.of().getDataDimension(filename);

        LinkedList<Node> path = pathService.findPath(graph);
        printResult(graph, path, dataDimension);
    }

    /**
     * Helper Method that prints the finished result to the console.
     */
    private void printResult(Graph graph, LinkedList<Node> path, int[] dataDimension) {
        StringBuilder generalOutput = new StringBuilder();
        StringBuilder longestPath = new StringBuilder();
        StringBuilder shortestPath = new StringBuilder();
        StringBuilder result = new StringBuilder();

        generalOutput
                .append("Gebiet mit ")
                .append(dataDimension[0])
                .append(" x ")
                .append(dataDimension[1])
                .append(" Zellen.")
                .append("\n")
                .append("Startzelle: ")
                .append(graph.getStart().getX())
                .append(",")
                .append(graph.getStart().getY());

        longestPath
                .append("\n")
                .append("Abschaetzung der Kostenobergrenze: ")
                .append(printService.extractLongestPathCost(path))
                .append(" KE");

        shortestPath
                .append("Minimalkosten: ")
                .append(printService.extractPathCost(graph))
                .append(" KE")
                .append("\n")
                .append("Weg: ")
                .append(printService.extractPath(graph));

        result.append(
                pathService instanceof LongestPathImpl ?
                        generalOutput.append(longestPath) :
                        shortestPath
                );

        System.out.println(result);
    }

}
