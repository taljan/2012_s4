package data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Graph {

    private Node start;
    private Node goal;

    private final Map<Node, Set<Edge>> nodeMap;

    private Graph() {
        this.nodeMap = new HashMap<>();
    }

    /**
     * Creates an empty Graph.
     * @return Graph - empty
     */
    public static Graph of() {
        return new Graph();
    }

    public boolean hasNode(Node node) {
        return nodeMap.containsKey(node);
    }

    public void addNode(Node node) {
        if (!hasNode(node)) {
            nodeMap.put(node, new HashSet<>());
        }
    }

    public void addEdge(Edge edge) {
        Node from = edge.getFrom();
        Node to = edge.getTo();

        validateNode(from);
        validateNode(to);

        nodeMap.get(from).add(edge);
    }

    private void validateNode(Node node) {
        if (!hasNode(node)) {
            throw new IllegalArgumentException(node.toString() + " is not a node");
        }
    }

    public Map<Node, Set<Edge>> getNodeMap() {
        return nodeMap;
    }

    public Node getStart() {
        return start;
    }

    public void setStart(Node start) {
        this.start = start;
    }

    public Node getGoal() {
        return goal;
    }

    public void setGoal(Node goal) {
        this.goal = goal;
    }

}
