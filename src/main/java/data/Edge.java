package data;

public class Edge {

    private Node from;
    private Node to;
    private double weight;

    private Edge(Node from, Node to, double weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public static Edge of(Node from, Node to, double weight) {
        return new Edge(from, to, weight);
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public double getWeight() {
        return weight;
    }

}
