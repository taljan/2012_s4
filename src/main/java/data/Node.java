package data;

import com.google.common.collect.ComparisonChain;

public class Node implements Comparable<Node> {

    private final int x;
    private final int y;
    private double weight;

    // total cost of heuristic - g(node) + h(node)
    private double f;

    // cost from start node to current node.
    private double g;

    // actual heuristic cost - distance from current Node to goalNode
    private double h;

    private Node predecessor;

    private Node(int x, int y, double weight) {
        this.x = x;
        this.y = y;
        this.weight = weight;
    }

    public static Node of(int x, int y, double weight) {
        return new Node(x, y, weight);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getF() {
        return f;
    }

    public void setF(double weight, double heuristic) {
        this.f = weight + heuristic;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public Node getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(Node predecessor) {
        this.predecessor = predecessor;
    }

    @Override
    public String toString() {
        return "Node{" +
                "x=" + x +
                ", y=" + y +
                ", weight=" + weight +
                ", f=" + f +
                ", g=" + g +
                ", h=" + h +
                '}';
    }

    @Override
    public int compareTo(Node o) {
        return ComparisonChain.start()
                .compare(this.getF(), o.getF())
                .compare(this.getH(), o.getH())
                .compare(this.getG(), o.getG())
                .result();
    }

}
