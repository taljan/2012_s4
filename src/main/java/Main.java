import service.PathService;
import service.impl.LongestPathImpl;
import service.impl.ShortestPathImpl;

public class Main {

    public static void main(String[] args) {
        PathService shortestPath = new ShortestPathImpl();
        PathService longestPath = new LongestPathImpl();

        if (args.length == 0) {
            System.out.println("No file is provided in the arguments.");
        }

        Simulation.of(longestPath).simulate(args[0]);
        Simulation.of(shortestPath).simulate(args[0]);
    }

}
