package service;

import data.Graph;
import data.Node;

import java.util.LinkedList;

/**
 * Service for only printing any result.
 */
public interface PrintService {

    int extractPathCost(Graph graph);

    int extractLongestPathCost(LinkedList<Node> path);

    String extractPath(Graph graph);

}
