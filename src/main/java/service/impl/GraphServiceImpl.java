package service.impl;

import data.Graph;
import data.Node;
import service.GraphService;
import service.NeighborService;

import java.util.List;

public class GraphServiceImpl implements GraphService {

    private final NeighborService neighborService;

    public GraphServiceImpl() {
        this.neighborService = new NeighborServiceImpl();
    }

    /**
     * Creates Nodes without Edges.
     * Data is passed as a single line from the data, so we just create
     * all the nodes without their neighbors. Hence all neighbors are still
     * not available for the method to parse them.
     *
     * @param data - Lines that have been read from the FileReader.
     * @return - a Graph without any Edges.
     */
    @Override
    public Graph createGraphWithoutEdges(List<String> data) {
        Graph graph = Graph.of();

        for (int y = 1; y < data.size(); y++) { // begin with 1 to ignore the comment section
            String[] dataWithoutComma = data.get(y).split(",");
            for (int x = 0; x < dataWithoutComma.length; x++) {

                if (dataWithoutComma[x].equals("S")) {
                    dataWithoutComma[x] = "0";
                }

                if (dataWithoutComma[x].equals("Z")) {
                    dataWithoutComma[x] = "-1";
                }

                double weight = Double.parseDouble(dataWithoutComma[x]);
                Node generatedNode = Node.of(x + 1, y, weight);

                if (generatedNode.getWeight() == 0) graph.setStart(generatedNode);
                if (generatedNode.getWeight() == -1) graph.setGoal(generatedNode);

                graph.addNode(generatedNode);
            }
        }

        return graph;
    }

    /**
     * Creates neighbors for each node in the graph.
     * @param graph - All Nodes without Edges.
     */
    @Override
    public void addNeighbors(Graph graph) {
        // Edges are still not initialized hence we are doing it after this method.
        graph.getNodeMap().forEach((node, edges) -> addNeighborsOnDirections(graph, node));
    }

    /**
     * Adds the neighbor to the corresponding Node according to it's current position.
     *
     * directions are used as a search query. We pass the current Node and search for a node
     * with e.g Node.x - directions[1]. That's how we find the left neighbor of the current node.
     *
     * @param graph - is necessary to find the neighbor of the current node in the graph.
     * @param currentNode - is necessary to get current Node in the graph.
     */
    private void addNeighborsOnDirections(Graph graph, Node currentNode) {
        int[][] directions = new int[][] {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        int y = 0;
        int x = 1;

        for (int[] direction : directions) {
            neighborService.createNeighbor(currentNode, graph, direction[y], direction[x]);
        }
    }

}
