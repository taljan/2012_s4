package service.impl;

import data.Edge;
import data.Graph;
import data.Node;
import service.PathService;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class LongestPathImpl implements PathService {

    /**
     * Finds one of the possible longest paths.
     * @param graph - to search for a path
     * @return - path from start to goal.
     */
    @Override
    public LinkedList<Node> findPath(Graph graph) {
        Queue<Node> open = new PriorityQueue<>();
        LinkedList<Node> closed = new LinkedList<>();

        Node start = graph.getStart();
        Node goalNode = graph.getGoal();
        initStartAndGoalNode(open, start, goalNode);

        while (!open.isEmpty()) {
            Node current = open.poll();
            for (Edge edge : graph.getNodeMap().get(current)) {
                Node neighbor = edge.getTo();

                if (current.equals(goalNode)) {
                    return closed;
                }

                int x = 0;
                int y = 1;

                if (isGoalInPositiveDirection(x, current, goalNode)) {
                    if (neighbor.getX() > current.getX()) {
                        closed.add(neighbor);
                        open.add(neighbor);
                    }
                } else if (isGoalInNegativeDirection(x, current, goalNode)) {
                    if (neighbor.getX() < current.getX()) {
                        closed.add(neighbor);
                        open.add(neighbor);
                    }
                }

                if (isGoalInPositiveDirection(y, current, goalNode)) {
                    if (isXDeltaCompleted(current, goalNode)) {
                        if (neighbor.getY() > current.getY()) {
                            closed.add(neighbor);
                            open.add(neighbor);
                        }
                    } else if (isGoalInNegativeDirection(y, current, goalNode)) {
                        if (neighbor.getY() < current.getY()) {
                            closed.add(neighbor);
                            open.add(neighbor);
                        }
                    }
                } else if (isGoalInNegativeDirection(y, current, goalNode))
                    if (isXDeltaCompleted(current, goalNode)) {
                        if (neighbor.getY() < current.getY()) {
                            closed.add(neighbor);
                            open.add(neighbor);
                        }
                    }
            }
        }

        return closed;
    }

    /**
     * All private methods are here to make the longestpath algorithm more readable.
     */

    private void initStartAndGoalNode(Queue<Node> open, Node start, Node goal) {
        start.setPredecessor(start);
        start.setG(0);
        goal.setWeight(0);

        open.add(start);
    }

    private boolean isXDeltaCompleted(Node current, Node goal) {
        return current.getX() == goal.getX();
    }

    private boolean isGoalInNegativeDirection(int i, Node current, Node goalNode) {
        return deltaDirections(current, goalNode)[i] < 0;
    }

    private boolean isGoalInPositiveDirection(int i, Node current, Node goalNode) {
        return deltaDirections(current, goalNode)[i] > 0;
    }

    private int[] deltaDirections(Node current, Node goal) {
        int[] deltaDirections = new int[2];
        int dx = goal.getX() - current.getX();
        int dy = goal.getY() - current.getY();

        deltaDirections[0] = dx;
        deltaDirections[1] = dy;

        return  deltaDirections;
    }

}
