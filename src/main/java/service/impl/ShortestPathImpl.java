package service.impl;

import data.Edge;
import data.Graph;
import data.Node;
import service.PathService;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class ShortestPathImpl implements PathService {

    /**
     * Uses A* Algorithm to calculate the shortest path.
     * @param graph - Graph to search the path for.
     * @return - the finished path
     */
    @Override
    public LinkedList<Node> findPath(Graph graph) {
        Queue<Node> open = new PriorityQueue<>();
        LinkedList<Node> closed = new LinkedList<>();
        Node start = graph.getStart();
        Node goal = graph.getGoal();

        start.setPredecessor(null);
        start.setG(0);

        open.add(start);

        while (!open.isEmpty()) {
            Node current = open.poll();

            for (Edge  edge : graph.getNodeMap().get(current)) {
                Node neighbor = edge.getTo();

                if (!open.contains(neighbor) && !closed.contains(neighbor)) {
                    neighbor.setPredecessor(current);
                    neighbor.setG(getGFromStartToCurrentNode(neighbor));
                    neighbor.setH(h(neighbor, goal));
                    neighbor.setF(neighbor.getG(), neighbor.getH());
                    open.add(neighbor);
                    if (open.contains(neighbor) && neighbor.getF() > (current.getF() + edge.getWeight())) {
                        neighbor.setPredecessor(current);
                        neighbor.setG(getGFromStartToCurrentNode(neighbor) + edge.getWeight());
                        neighbor.setF(neighbor.getG(), neighbor.getH());
                    }
                }
            }

            closed.add(current);
        }

        return closed;
    }

    /**
     * Calculates the g cost of current node.
     * @param current - current node we investigate.
     * @return - g cost of current node.
     */
    private int getGFromStartToCurrentNode(Node current) {
        int g = 0;
        Node predecessor = current.getPredecessor();

        g += current.getWeight();
        while (predecessor != null) {
            g += predecessor.getWeight();
            predecessor = predecessor.getPredecessor();
        }

        return g;
    }


    /**
     * Calculates the h cost of the current node.
     * @param current - the current node we investigate.
     * @param goal - goal node to calculate the distance between current node and goal.
     * @return the cost of h function.
     */
    private int h(Node current, Node goal) {
        int dx = Math.abs(current.getX() - goal.getX());
        int dy = Math.abs(current.getY() - goal.getY());

        return dx + dy;
    }

}
