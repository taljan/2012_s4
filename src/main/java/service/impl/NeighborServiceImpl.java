package service.impl;

import data.Edge;
import data.Graph;
import data.Node;
import service.NeighborService;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class NeighborServiceImpl implements NeighborService {

    /**
     * Finds the neighbor of the current Node and adds it ot it's Edge(Neighbors) list.
     * @param currentNode
     * @param graph
     * @param yDirectionOffset
     * @param xDirectionOffset
     */
    @Override
    public void createNeighbor(Node currentNode, Graph graph, int yDirectionOffset, int xDirectionOffset) {
        Node neighbor = extractNeighbor(graph, currentNode, yDirectionOffset, xDirectionOffset);

        Optional.ofNullable(neighbor)
                .ifPresent(nbr -> graph.addEdge(Edge.of(currentNode, nbr, nbr.getWeight())));
    }

    /**
     * Helper function to find the neighbor of the current node according to its position.
     * (search query = yDirectionOffset, xDirectionOffset)
     * @param graph - Graph to search in the nodes
     * @param parentNode - is the currentNode we are searching the neighbors from.
     * @param yDirectionOffset - search Query for y direction
     * @param xDirectionOffset - search Query for x direction
     * @return neighbor of parent/current node.
     */
    private Node extractNeighbor(Graph graph, Node parentNode, int yDirectionOffset, int xDirectionOffset) {
        Optional<Map.Entry<Node, Set<Edge>>> existingNode = graph.getNodeMap()
                .entrySet()
                .stream()
                .filter(nodeSetEntry ->
                        (nodeSetEntry.getKey().getY() == parentNode.getY() + yDirectionOffset) &&
                                (nodeSetEntry.getKey().getX() == parentNode.getX() + xDirectionOffset))
                .findFirst();

        return existingNode.map(Map.Entry::getKey).orElse(null);
    }
}
