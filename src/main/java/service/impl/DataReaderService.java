package service.impl;

import data.Graph;
import service.GraphService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * DataRederService
 * only responsible for reading the incoming data.
 */
public class DataReaderService {

    private final GraphService graphService;

    private DataReaderService() {
        this.graphService = new GraphServiceImpl();
    }

    public static DataReaderService of() {
        return new DataReaderService();
    }

    /**
     * Reading the data from the given file and returns a Graph.
     * @param fileName - Filename to the file to be read.
     * @return - return a Graph with all its Nodes and Edges.
     */
    public Graph getData(String fileName) {
        checkNotNull(fileName);
        List<String> strings = null;

        try {
            strings = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Graph graph = graphService.createGraphWithoutEdges(strings);
        graphService.addNeighbors(graph);

        return graph;
    }

    /**
     * Reads the dimension from File and return an array with its values.
     * Converts the values from String to Integer values, saves them
     * to an 1D Array with index 0 = x, index 1 = y.
     *
     * This Method is only necessary for printing the dimensions.
     *
     * @param filename - String filename
     * @return int[] - with dimensions x,y of the field.
     */
    public int[] getDataDimension(String filename) {
        checkNotNull(filename);
        List<String> strings = null;

        try {
            strings = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String dimString = strings.get(0);
        int[] dimensions = new int[2];

        StringBuilder sb = new StringBuilder();
        char[] charArray = dimString.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if (Character.isDigit(c)) {
                if (Character.isDigit(charArray[i + 1])) {
                    sb.append(c);
                } else {
                    sb.append(c).append(",");
                }
            }
        }
        final String[] split = sb.toString().split(",");

        dimensions[0] = Integer.parseInt(split[0]);
        dimensions[1] = Integer.parseInt(split[1]);

        return dimensions;
    }

}
