package service.impl;

import data.Graph;
import data.Node;
import service.PrintService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PrintServiceImpl implements PrintService {

    /**
     * calculates out the total path costs.
     * @param graph - Graph where the path exists.
     * @return the path cost
     */
    @Override
    public int extractPathCost(Graph graph) {
        int cost = 0;

        Node predecessor = graph.getGoal().getPredecessor();
        while (predecessor != null) {
            cost += predecessor.getWeight();
            predecessor = predecessor.getPredecessor();
        }

        return cost;
    }

    /**
     * calculates the total path of the possible longest path.
     * @param path - the path to calculate the costs.
     * @return the path cost for the longest possible path.
     */
    @Override
    public int extractLongestPathCost(LinkedList<Node> path) {
        return path.stream().mapToInt(node -> (int) node.getWeight()).sum();
    }

    /**
     * Builds the string from the found path in Graph.
     * @param graph - the graph where the path is in.
     * @return - path as a string.
     */
    @Override
    public String extractPath(Graph graph) {
        StringBuilder pathBuilder = new StringBuilder();
        Node goal = graph.getGoal();

        List<Node> finalPath = new ArrayList<>();

        Node predecessor = goal.getPredecessor();
        finalPath.add(goal);
        while (predecessor != null) {
            finalPath.add(predecessor);
            predecessor = predecessor.getPredecessor();
        }

        LinkedList<Node> nodes = new LinkedList<>(finalPath);
        nodes
                .descendingIterator()
                .forEachRemaining(node -> pathBuilder.append(node.getX()).append(",").append(node.getY()).append("; "));

        return pathBuilder.toString();
    }

}
