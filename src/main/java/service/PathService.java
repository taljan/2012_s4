package service;

import data.Graph;
import data.Node;

import java.util.LinkedList;

public interface PathService {

    LinkedList<Node> findPath(Graph graph);

}
