package service;

import data.Graph;
import data.Node;

/**
 * Only responsible for creating neighbors.
 * Extracted this because creating neighbors seemed to be more complex than tought.
 */
public interface NeighborService {

    void createNeighbor(Node currentNode, Graph graph, int yDirection, int xDirection);

}
