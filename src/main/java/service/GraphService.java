package service;

import data.Graph;

import java.util.List;

/**
 * Service is responsible for graph relevant graph creating logic.
 */
public interface GraphService {

    Graph createGraphWithoutEdges(List<String> data);

    void addNeighbors(Graph graph);

}
